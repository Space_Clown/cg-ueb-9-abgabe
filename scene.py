
from OpenGL.GL import *
from OpenGL.GLU import *
from OpenGL.GLUT import *

import numpy as np


class Scene:
    """ OpenGL 2D scene class """

    # initialization
    def __init__(self, width, height,
                 scenetitle="Bier Curve Template"):
        self.scenetitle = scenetitle
        self.pointsize = 7
        self.linewidth = 5
        self.width = width
        self.height = height
        self.points = []
        self.lines = []
        self.points_on_bezier_curve = []
        self.controllpoints = [] # why write it with double 'l' ?!
        self.knotvector = []
        self.weight = []

        self.degree = 3
        self.m = 0.1

        self.change = False

    # set scene dependent OpenGL states
    def setOpenGLStates(self):
        glPointSize(self.pointsize)
        glLineWidth(self.linewidth)
        glEnable(GL_POINT_SMOOTH)

    # render
    def render(self):
        glPolygonMode(GL_FRONT_AND_BACK, GL_LINE)

        # set foreground color to black
        glColor(0.0, 0.0, 0.0)

        # render all points
        glBegin(GL_POINTS)
        for p in self.controllpoints:
            glVertex2fv(p)
        glEnd()

        if len(self.points) >= 2:
            # render polygon
            glLineWidth(self.linewidth)
            glBegin(GL_LINE_STRIP)
            for p in self.points:
                glVertex2fv(p)
            glEnd()

            # render bezier curve
            glBegin(GL_LINE_STRIP)
            for p in self.points_on_bezier_curve:
                glColor(0.7, 0.7, 0.7)
                glVertex2fv(p)
            glEnd()

            # points bezier curve
            glBegin(GL_POINTS)
            for p in self.points_on_bezier_curve:
                glColor(1.0, 0.0, 0.3)
                glVertex2fv(p)
            glEnd()

            # set flag for better debug reasons
            # actually found out it has better performance now.
            if len(self.points) >= 2 and self.change:
                self.determine_points_on_bezier_curve()
                self.change = False

    def add_point(self, point):
        self.points.append(point)
        self.controllpoints.append(point)
        self.weight.append(1)
        self.getKnotVector()

        if len(self.points) >= 2:
            self.determine_points_on_bezier_curve()


    # clear polygon
    def clear(self):
        # Reset everything I might have broken...
        self.points = []
        self.points_on_bezier_curve = []
        self.controllpoints = []
        self.knotvector = []
        self.degree = 3
        self.m = 0.1


    def determine_points_on_bezier_curve(self):
        t = 0
        self.points_on_bezier_curve = []

        # Why do Java developers wear glasses?
        # Because the don't C#!
        # Eventuell bekomme ich noch einen Punkt fuer den gelungenen Witz? :)

        while t < self.knotvector[-1]:
            for r in range(len(self.knotvector) - 1):
                if self.knotvector[r] > t:
                    p = self.deboor(r - 1, self.degree - 1, t)
                    self.points_on_bezier_curve.append(p)
                    t += 1 * self.m
                    break

    def deboor(self, i, j, t):
        if j == 0:
            b = self.controllpoints[i] * np.array(self.weight[i])
        else:
            alpha = ( t - self.knotvector[i] ) / ( self.knotvector[i - j + self.degree] - self.knotvector[i] )
            b = (1 - alpha) * np.array(self.deboor(i - 1, j - 1, t)) + alpha * np.array(self.deboor(i, j - 1, t))
        return b

    def getKnotVector(self):
        self.knotvector = []
        lenCon = len(self.controllpoints)

        # start with Zeros
        for n in range(self.degree):
            self.knotvector.append(0)

        # Fill up with normal numbers
        for n in range(1, (lenCon - 1 - (self.degree - 2))):
            self.knotvector.append(n)

        #finish with degree-times the last number
        for n in range(self.degree):
            self.knotvector.append(lenCon - (self.degree - 1))


    def change_degree(self, mod):
        if mod == 0:
            if self.degree != 2:
                self.degree -= 1
                self.change = True
        else:
            if self.degree < len(self.controllpoints):
                self.degree += 1
                self.change = True

    def change_anzahl(self, mod):
        if mod == 0:
            self.m *= 0.5
            self.change = True
        else:
            if (self.m > 1):
                return
            else:
                self.m *= 2
                self.change = True







